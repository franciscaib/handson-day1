import java.util.Scanner;

public class CharacterizedString{
    public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
        System.out.print("Ketik kata/kalimat lalu tekan enter :");
        String nama = input.nextLine();
        System.out.println("=======================================");
        System.out.println("String penuh : " + nama);
        System.out.println("=======================================");
        System.out.println("Proses memecahkan kalimat menjadi karakter...");
        System.out.println("...........");
        System.out.println("Done");
        System.out.println("=======================================");
        char[] c = new char[nama.length()];
        for (int i = 0; i < nama.length(); i++) {
            System.out.println("Karakter["+i+"] : "+ nama.substring(i,i+1 ));
        }
        System.out.println("=======================================");
    }
}